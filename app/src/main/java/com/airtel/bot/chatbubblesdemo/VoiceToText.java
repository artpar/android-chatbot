package com.airtel.bot.chatbubblesdemo;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.airtel.bot.chatbubblesdemo.voice.WavRecorder;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.ByteBuffer;

public class VoiceToText {


    private byte[] byteArray;
    private Socket socket;
    private DataOutputStream dOut;
    private static String mFileName = null;
    private Exception networkException;

    private String serverAddr;

    private int serverPort;

    private WavRecorder wavRecorder;

    private boolean connectedFlag, recordingFlag = false;

    public interface VoiceToTextCallbackHandler {
        void OnVoiceToText(String message);
    }

    private VoiceToTextCallbackHandler handler;


    public VoiceToText(Context context, VoiceToTextCallbackHandler handler, String serverAddress, Integer port) {
        this.handler = handler;
        mFileName = context.getExternalCacheDir().getAbsolutePath();
        mFileName += "/temp_file.wav";
        this.serverAddr = serverAddress;
        this.serverPort = port;
    }

    public boolean isRecording() {
        return recordingFlag;
    }

    public boolean isConnected() {
        return connectedFlag;
    }

    public void connect() {
        Thread socketThread = new Thread(new ClientThread());
        socketThread.start();
    }

    private class ClientThread implements Runnable {

        @Override
        public void run() {
            try {
                socket = new Socket(serverAddr, serverPort, true);
                dOut = new DataOutputStream(socket.getOutputStream());
                connectedFlag = true;
            } catch (IOException e) {
                Log.d("VoiceToText", "Failed to connect to server", e);
                networkException = e;
                connectedFlag = false;
            }
        }
    }

    public void recordVoice() throws Exception {

        if (!connectedFlag) {
            if (networkException != null) {
                throw networkException;
            } else {
                throw new Exception("not connected to server");
            }
        }

        if (recordingFlag) {
            return;
        }

        byteArray = null;
        recordingFlag = true;
        wavRecorder = new WavRecorder(mFileName);
        wavRecorder.startRecording();
    }

    public void stopRecording() {
        if (!recordingFlag) {
            return;
        }
        if (!connectedFlag) {
            return;
        }
        recordingFlag = false;
        wavRecorder.stopRecording();

        readFile();
    }

    private void readFile() {
        File file = new File(mFileName);
        int size = (int) file.length();

        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            byteArray = bytes;
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new SendDataAsynch().execute();
    }


    private String sendData() {
        try {

            int bufferLength = byteArray.length;

            ByteBuffer byteBuffer = ByteBuffer.allocate(bufferLength + 4);

            byteBuffer.putInt(0, bufferLength);

            int i = 4;
            for (byte s : byteArray) {
                byteBuffer.put(i, s);
                i++;
            }

            dOut.write(byteBuffer.array());

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String response = in.readLine();

            if (!android.text.TextUtils.isEmpty(response)) Log.d("RESPONSE", response);

            dOut.flush();

            return response;


        } catch (IOException e) {
            e.printStackTrace();
            connectedFlag = false;
        }
        return null;
    }


    private class SendDataAsynch extends AsyncTask<Void, Void, Void> {
        String response;

        @Override
        protected Void doInBackground(Void... voids) {
            response = sendData();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!TextUtils.isEmpty(response)) {
                handler.OnVoiceToText(response);
            }
        }
    }


}
